import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';





@Component({
  selector: 'app-weddings',
  templateUrl: './weddings.component.html',
  styleUrl: './weddings.component.css'
})
export class WeddingsComponent {

}



@NgModule({

  imports: [
    BrowserModule,
    BrowserAnimationsModule,  // Required for some Angular Material components
    MatCardModule
  ],
 
})
export class AppModule { }
