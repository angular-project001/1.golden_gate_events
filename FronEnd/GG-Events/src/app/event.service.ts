import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http:HttpClient ) { }

  getAllEvents(){
    return this.http.get('http://localhost:8090/getAllEvents');
  }

  getEventsByCategory(category: string){
    return this.http.get('http://localhost:8090/getEventsByCategory/'+category).toPromise();
  }

  getEventsByName(name: string){
    return this.http.get('http://localhost:8090/getEventsByName/'+name);
  }

  addEvent(Events:any){
    return this.http.post('http://localhost:8090/addEvent', Events);
  }
  
  updateEvent(Event:any){
    return this.http.put('http://localhost:8090/updateEvent', Event);
  }

  deleteEvent(pId:number){
    return this.http.delete('http://localhost:8090/deleteEventById/'+pId);
  }
}
