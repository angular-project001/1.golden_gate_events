import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../event.service';
import { CustomerService } from '../customer.service';
import { BookingService } from '../Booking.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-search-event',
  templateUrl: './search-event.component.html',
  styleUrl: './search-event.component.css'
})
export class SearchEventComponent implements OnInit {
addToBooking(_t6: any) {
throw new Error('Method not implemented.');
}

  eventName: any;
  categoryName:any;
  events:any;
  customerId:any;
  loginStatus:any;

  constructor(private route: ActivatedRoute,
    private eventService:EventService,
    private customerService : CustomerService,
    private bookingService :BookingService,
    private router : Router,
    private toastr : ToastrService
  ){}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.eventName = params['eventName'];
      // this.categoryName = params['categoryName'];
      this.searchEventByName(this.eventName);
      // this.searcheventByCategory(this.categoryName);
    });
  }

  searchEventByName(name: string){
    this.eventService.getEventsByName(name).subscribe((data:any)=>{
      this.events = data; 
    })
  }
  async searcheventByCategory(category: string){
    await this.eventService.getEventsByCategory(category).then((data:any)=>{
      this.events = data; 
    })
  }


  
  addToCart(event: any): void {
    this.customerId = localStorage.getItem('customerId');
    console.log("CustomerIDDD.. "+this.customerId);
    this.loginStatus = this.customerService.getCustomerLoginStatus();
    console.log("Login AStatus : "+this.loginStatus);
    
    if(this.loginStatus){
      const cartItem = {
        imgSrc:event.imgsrc,
        name: event.name,
        price: event.price,
        quantity: 1 // You can set the initial quantity here
      };
      this.bookingService.addToBooking(cartItem).subscribe(response => {
        // Handle success or error response from the server
        // console.log(response);
        this.toastr.success(
          "event Added", 
          '', 
          { timeOut:1000, progressBar : true, progressAnimation : 'increasing'
        });
      });
    }else{
      this.router.navigate(['login']);
    }
  }
}
