import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  customerId:any;

  constructor(private http: HttpClient) {  }

  getBookingItems(c_id:any){
    return this.http.get('http://localhost:8090/getBookingItems/'+c_id);
  }

  addToBooking(BookingItem:any) {
    this.customerId = localStorage.getItem('customerId');
    return this.http.post('http://localhost:8090/addToBooking/'+this.customerId, BookingItem);
  }

  removeFromBooking(BookingId: number) {
    return this.http.delete(`http://localhost:8090/removeFromBooking/${BookingId}`);
  }

  updateBookingItem(item: any) {
    return this.http.put(`http://localhost:8090/updateBookingItem/${item.BookingId}`, item);
  }

}
