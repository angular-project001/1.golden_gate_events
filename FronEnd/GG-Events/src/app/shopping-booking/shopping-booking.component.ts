import { Component, OnInit } from '@angular/core';
import { BookingService } from '../Booking.service';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shopping-Booking',
  templateUrl: './shopping-Booking.component.html',
  styleUrl: './shopping-Booking.component.css'
})
export class ShoppingBookingComponent implements OnInit {
  BookingItems: any;
  emailId:any;
  customerId:any;
  BookingValue:number = 0;
  totalBookingValue: number = 0;
  BookingLength:number = 0;
  gst:number=0;
  deliveryCharges:number=0;

  constructor(private BookingService: BookingService, private customerservice :CustomerService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getBookingItems();

  }

  getBookingItems(): void {
    this.BookingItems = [];
    this.emailId = localStorage.getItem('emailId');
    this.customerId = localStorage.getItem('customerId');
    this.BookingService.getBookingItems(this.customerId).subscribe((BookingItems:any) => {
      this.BookingItems = BookingItems; 
      this.calculateTotal();   
      this.BookingLength = this.BookingItems.length; 
    });
  }

  removeFromBooking(BookingId:any){
    console.log("BookingItemId "+ BookingId);
    
    this.BookingService.removeFromBooking(BookingId).subscribe(()=>{
      this.getBookingItems();
    })
  }

  
  incrementQuantity(item: any): void {
    console.log("item : "+ item.bookingId);
    console.log("p_id : "+ item.pId);
    console.log("ONly Item : "+ item);
    console.log("ONly Item : "+ item.customer);
    
    
    item.quantity++;
    this.BookingService.updateBookingItem(item).subscribe(() => {
      this.getBookingItems();
    });
  }

  decrementQuantity(item: any): void {
    if (item.quantity > 1) {
      item.quantity--;
      this.BookingService.updateBookingItem(item).subscribe(() => {
        this.getBookingItems();
      });
    }
  }

  calculateTotal(): void {
    this.BookingValue = 0;
    for (const item of this.BookingItems) {
      this.BookingValue += item.quantity * item.price;
    }
    if(this.BookingValue <= 30000 && this.BookingValue > 0){
      this.deliveryCharges = 50;
    }else{
      this.deliveryCharges = 0;
    }
    this.totalBookingValue = this.BookingValue + this.deliveryCharges;
  }
  placeOrder(){
    this.toastr.success(
      "",
      "Order Placed Successfully",
      {timeOut:2000, progressBar : true, progressAnimation : 'increasing'});
  }
}