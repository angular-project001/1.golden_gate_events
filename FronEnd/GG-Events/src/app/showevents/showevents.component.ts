import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { EventService } from '../event.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-showevents',
  templateUrl: './showevents.component.html',
  styleUrl: './showevents.component.css'
})
export class ShoweventsComponent implements OnInit {

  category:any;
  events:any;

  editevent:any;
  serviceEvent: any;

  constructor(private serviceEvents: EventService,
    private toastr:ToastrService
  ){  
    this.editevent={
      pId:'',
      name:'',
      imgsrc:'',
      description:'',
      category:'',
      noOfMembers:'0',
      price:''
    }
  }

  ngOnInit(): void {
    // this.serviceProducts.getAllProducts().subscribe((data:any)=>{
    //   this.products = data;
    // })
  }
  async getEventByCategory(category:any){
    console.log(category);
    this.events = null;
    await this.serviceEvents.getEventsByCategory(category).then((data:any)=>{
      this.events = data;
    })
  }
  editEvent(event:any){
    this.editevent = event
    this.serviceEvents.updateEvent(this.editevent).subscribe((data:any)=>{
      console.log(data); 
      this.toastr.success(
        "Edit Status",
        "Event Edited Successfully",
        { timeOut:1000, progressBar : true, progressAnimation : 'increasing'});     
    });
  }

  deleteEvent(pId:number){
    console.log(pId);
    this.serviceEvents.deleteEvent(pId).subscribe((data: any)=>{
      console.log(data);
    });

    const i = this.events.findIndex((element:any) =>{
      return pId = element.pId;
    })
    this.events.splice(i,1);
  }
}
