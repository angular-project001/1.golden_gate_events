import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent {
  constructor(private http: HttpClient) {}

  onSubmit(form: HTMLFormElement) {
    const formData = new FormData(form);
    this.http.post<any>('http://localhost:8090/submit-event', formData)
      .subscribe(
        response => {
          console.log('Event registered successfully', response);
          alert('Event registered successfully');
          form.reset();
        },
        error => {
          console.error('Error registering event', error);
          alert('Error registering event');
        }
      );
  }
}
