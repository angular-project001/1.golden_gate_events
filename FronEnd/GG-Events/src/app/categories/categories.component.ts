import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BookingService } from '../Booking.service';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrl: './categories.component.css'
})
export class CategoriesComponent implements OnInit {

  Events:any;
  category:any;
  customerId:any;
  loginStatus:any;

  constructor(private EventService: EventService,
    private route: ActivatedRoute, 
    private customerService: CustomerService,
    private router: Router,
    private bookingService: BookingService,
    private toastr : ToastrService
    ){ }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.category = params['category'];
      this.fetchEventsByCategory(this.category);
    });
  }

  async fetchEventsByCategory(category: string) {
    // Call Eventservice to fetch Events based on the category
    await this.EventService.getEventsByCategory(category).then((Events: any) => {
      this.Events = Events;
    });
  }

 
  addTobooking(event: any): void {
    this.customerId = localStorage.getItem('customerId');
    console.log("CustomerIDDD.. "+this.customerId);
    this.loginStatus = this.customerService.getCustomerLoginStatus();
    console.log("Login AStatus : "+this.loginStatus);
    
    if(this.loginStatus){
      const bookingItem = {
        imgSrc:event.imgsrc,
        name: event.name,
        price: event.price,
        noOfMembers: 1 // You can set the initial quantity here
      };
      this.bookingService.addToBooking(bookingItem).subscribe((response: any) => {
        // Handle success or error response from the server
        // console.log(response);
        this.toastr.success(
          "event Added", 
          '', 
          { timeOut:1000, progressBar : true, progressAnimation : 'increasing'
        });
      });
    }else{
      this.router.navigate(['login']);
    }
  }


}
