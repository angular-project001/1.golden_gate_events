import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';
import { BookingService } from '../Booking.service';
import { Router, mapToCanActivate } from '@angular/router';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-Events',
  templateUrl: './Events.component.html',
  styleUrl: './Events.component.css'
})
export class EventsComponent implements OnInit {
  Events: any;
  customerId:any;
  loginStatus:any;
  


  constructor(private serviceEvents: EventService,
    private bookingService: BookingService, 
    private router:Router,
    private customerService: CustomerService,
    private toastr : ToastrService
    ){ }

  ngOnInit(): void {
    this.Events = null;
    this.serviceEvents.getAllEvents().subscribe((data: any)=>{
      this.Events = data;
    });
  }


  addTobooking(event: any): void {
    this.customerId = localStorage.getItem('customerId');
    console.log("CustomerIDDD.. "+this.customerId);
    this.loginStatus = this.customerService.getCustomerLoginStatus();
    console.log("Login AStatus : "+this.loginStatus);
    
    if(this.loginStatus){
      const bookingItem = {
        imgSrc:event.imgsrc,
        name: event.name,
        price: event.price,
        noOfMembers: 1 // You can set the initial quantity here
      };
      this.bookingService.addToBooking(bookingItem).subscribe(response => {
        // Handle success or error response from the server
        // console.log(response);
        this.toastr.success(
          "Event Added", 
          '', 
          { timeOut:1000, progressBar : true, progressAnimation : 'increasing'
        });
      });
    }else{
      this.router.navigate(['login']);
    }
    
  }

}
