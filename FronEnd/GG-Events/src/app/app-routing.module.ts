import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { EventsComponent } from './Events/Events.component';
import { CategoriesComponent } from './categories/categories.component';
import { SearchEventComponent } from './search-event/search-event.component';
import { ShoppingBookingComponent } from './shopping-booking/shopping-booking.component';
import { authGuard } from './auth.guard';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomeComponent } from './home/home.component';
import { ShowallcustomersComponent } from './showallcustomers/showallcustomers.component';
import { ShowcustomerbyidComponent } from './showcustomerbyid/showcustomerbyid.component';
import { AddeventComponent } from './addevent/addevent.component';
import { ShoweventsComponent } from './showevents/showevents.component';
import { CardsComponent } from './cards/cards.component';
import { WeddingsComponent } from './weddings/weddings.component';
import { BirthdaysComponent } from './birthdays/birthdays.component';
import { SpecialDaysComponent } from './special-days/special-days.component';
import { CorporateEventsComponent } from './corporate-events/corporate-events.component';
import { BookFormComponent } from './book-form/book-form.component';

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'forgotPassword', component: ForgotPasswordComponent },
  { path: 'events', component: EventsComponent },
  { path: 'categoryComponent', component: CategoriesComponent },
  { path: 'search', component: SearchEventComponent },
  { path: 'booking', component: ShoppingBookingComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: '', component: HomeComponent },
  { path: 'demos', component: CardsComponent },
  { path: 'Weddings', component: WeddingsComponent },
  { path: 'showallcustomers', component: ShowallcustomersComponent },
  { path: 'showcustomerbyid', component: ShowcustomerbyidComponent },
  { path: 'addevents', component: AddeventComponent },
  { path: 'showevents', component: ShoweventsComponent },
  { path: 'categoryComponent?category=Mobile', component: CategoriesComponent },
  { path: 'birthdays', component: BirthdaysComponent },
  { path: 'special-days', component: SpecialDaysComponent },
  { path: 'corporate-events', component: CorporateEventsComponent },
  {path: 'book-form', component: BookFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
