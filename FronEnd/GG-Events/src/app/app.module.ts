import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LogoutComponent } from './logout/logout.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { FooterComponent } from './footer/footer.component';
import { CategoriesComponent } from './categories/categories.component';
import { SearchEventComponent,  } from './search-event/search-event.component';
import { ShoppingBookingComponent,  } from './shopping-booking/shopping-booking.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomeComponent } from './home/home.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ShowallcustomersComponent } from './showallcustomers/showallcustomers.component';
import { ShowcustomerbyidComponent } from './showcustomerbyid/showcustomerbyid.component';
import { AddeventComponent,  } from './addevent/addevent.component';
import { ShoweventsComponent,  } from './showevents/showevents.component';
import { PlaceorderComponent } from './placeorder/placeorder.component';
import { BirthdaysComponent } from './birthdays/birthdays.component';
import { CorporateEventsComponent } from './corporate-events/corporate-events.component';
import { SpecialDaysComponent } from './special-days/special-days.component';
import { EventsComponent } from './Events/Events.component';
import { CardsComponent } from './cards/cards.component';
import { DemosComponent } from './demos/demos.component';
import { WeddingsComponent } from './weddings/weddings.component';
import { BookFormComponent } from './book-form/book-form.component';
import { PostCustomerComponent } from './post-customer/post-customer.component';
import { GetAllCustomersComponent } from './get-all-customers/get-all-customers.component';
import { UpdateCustomerComponent } from './update-customer/update-customer.component';
import { ModelsComponent } from './models/models.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
    ForgotPasswordComponent,
    FooterComponent,
    CategoriesComponent,
    SearchEventComponent,
    ShoppingBookingComponent,
    AboutusComponent,
    ContactUsComponent,
    HomeComponent,
    WeddingsComponent,
    ShowallcustomersComponent,
    ShowcustomerbyidComponent,
    AddeventComponent,
    ShoweventsComponent,
    PlaceorderComponent,
    EventsComponent,
    BirthdaysComponent,
    CorporateEventsComponent,
    SpecialDaysComponent,
    CardsComponent,
    DemosComponent,
    BookFormComponent,
    PostCustomerComponent,
    GetAllCustomersComponent,
    UpdateCustomerComponent,
    ModelsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    NgxCaptchaModule,
    HttpClientModule,
    ToastrModule.forRoot(),    // ToastrModule added
    BrowserAnimationsModule,  // required animations module
    ReactiveFormsModule,
    CarouselModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
