import { Component } from '@angular/core';
import { AdminService } from '../admin.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EventService } from '../event.service';

@Component({
  selector: 'app-addevent',
  templateUrl: './addevent.component.html',
  styleUrl: './addevent.component.css'
})
export class AddeventComponent {
addEvent(arg0: any) {
throw new Error('Method not implemented.');
}

  events:any;

  constructor(private adminService : AdminService,
    private serviceEvent: EventService,
     private toastr:ToastrService, 
     private router: Router){
    this.events={
      // pId:0,
      category:'',
      description:'',
      imgsrc:'',
      name:'',
      price:'',
      noOfMembers:0,
    }
  }

  addEvents(event : any){
    // this.products.pId = product.pId;
    this.events.category = event.category,
    this.events.description = event.description,
    this.events.imgsrc = event.imgsrc,
    this.events.name = event.name,
    this.events.price = event.price,
    this.events.noOfMembers = event.noOfMembers

    console.log(this.events)
    this.serviceEvent.addEvent(this.events).subscribe((data:any)=>{
      console.log(data);
      if(data != null){
        this.toastr.success(
          "", 
          'Event Added', 
          { timeOut:3000, progressBar : true, progressAnimation : 'increasing'
        });
      }
    });
    this.router.navigate(['events'])
  }
}
