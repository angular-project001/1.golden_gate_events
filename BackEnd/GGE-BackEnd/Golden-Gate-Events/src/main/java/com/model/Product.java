package com.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product {
	@Id@GeneratedValue
	private int id; 
    private String name;
    private String imgsrc; 
    private int categoryID;
    private	double cost;
    private String desciption;
    public Product(){
    	
    }
	public Product(int id, String name, String imgsrc, int categoryID ,String desciption,double cost) {
		super();
		this.id = id;
		this.name = name;
		this.imgsrc = imgsrc;
		this.categoryID = categoryID;
		this.desciption=desciption;
		this.cost=cost;
	}
	
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDesciption() {
		return desciption;
	}
	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImgsrc() {
		return imgsrc;
	}
	public void setImgsrc(String imgsrc) {
		this.imgsrc = imgsrc;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", imgsrc=" + imgsrc + ", categoryID=" + categoryID + ", cost="
				+ cost + ", desciption=" + desciption + "]";
	}
	
}