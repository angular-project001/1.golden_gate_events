package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

	@Query("from Product e where e.name = :name")
	List<Product> findByName(@Param("name") String productName);
	
	@Query("from Product e where e.categoryID = :categoryID")
	List<Product> findBycatogiryId(@Param("categoryID") int catogiryId);
	
}