package com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;


@RestController
@CrossOrigin(origins="http://localhost:4200")
public class ProductController {
	
	@Autowired
	ProductDao productDao;
	@GetMapping("getAllProducts")
	public List<Product> getAllProducts() {
		return productDao.getAllProducts();
	}
	@GetMapping("getById/{id}")
	public Product getProductById(@PathVariable("id") int productId) {
		return productDao.getProductById(productId);
	}
	
	@GetMapping("getProductByName/{name}")
	public List<Product> getProductByName(@PathVariable("name") String productName) {
		return productDao.getProductByName(productName);
	}
	
	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {
		return productDao.addProduct(product);
	}
	
	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		return productDao.updateProduct(product);
	}
	@GetMapping("getBycatogiryId/{catogiryId}")
	public List<Product> getBycatogiryId(@PathVariable("catogiryId") int catogiryId){
		return productDao.getBycatogiryId(catogiryId);
	}
	@DeleteMapping("deleteProductById/{id}")
	public String deleteProductById(@PathVariable("id") int productId) {
		productDao.deleteProductById(productId);
		return "Product Record Deleted Successfully!!!";
	}
}